import io
import sys

from typing import Union


class ClojureError(Exception):
    pass


# noinspection PyUnusedLocal
def run(input_: Union[io.TextIOWrapper, io.StringIO]=sys.stdin):
    pass


if __name__ == '__main__':
    run()
