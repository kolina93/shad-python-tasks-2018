import io
import os
import sys

import pytest

from . import clojure

sys.path.append('..')

from test_utils import check_output


SCRIPT_FILENAME = os.path.join(os.path.dirname(__file__), 'clojure.py')


def test_clojure_simple():
    check_output(
        SCRIPT_FILENAME,

        # stdin
        '(+ 1 2)\n',

        # stdout
        '3\n'
    )


def test_clojure_raiser_error():
    script = '('

    with pytest.raises(clojure.ClojureError):
        clojure.run(io.StringIO(script))
