## HELLO WORLD

### Условие

Нужно написать функцию, которая будет возвращать строчку Hello world!

### Пример
```python
>>> get_hello_world()
Hello world!
```

### Пример запуска тестов
Запускаются тесты из файла ```test_public.py```.
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/hello_world$ pytest 
```
