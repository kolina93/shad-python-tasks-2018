#include <Python.h>

#include <unordered_map>
#include <string>

static std::unordered_map<std::string, PyObject*> objects_keeper;

static PyObject* put_object(PyObject *self, PyObject *args) {
    const char* tag;
    PyObject* value;

    if (!PyArg_ParseTuple(args, "sO", &tag, &value)) {
      return NULL;
    }

    std::string key(tag);
    auto it = objects_keeper.find(key);

    if (it != objects_keeper.end()) {
        PyObject* old_value = it->second;
        Py_XDECREF(old_value);
        objects_keeper.erase(it);
    }

    Py_XINCREF(value);
    objects_keeper[key] = value;

    Py_RETURN_NONE;
}

static PyObject* get_object(PyObject *self, PyObject *args) {
    const char* tag;
    if (!PyArg_ParseTuple(args, "s", &tag)) {
        return NULL;
    }

    std::string key(tag);
    auto it = objects_keeper.find(key);

    if (it == objects_keeper.end()) {
        PyErr_SetString(PyExc_KeyError, "No such tag");
        return NULL;
    }

    PyObject* value = it->second;
    Py_XINCREF(value);
    return value;
}

static PyMethodDef functions[] = {
    {"put_object", put_object, METH_VARARGS, "Put object with tag"},
    {"get_object", get_object, METH_VARARGS, "Get object with tag"},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

static PyModuleDef module = {
    PyModuleDef_HEAD_INIT,
    "keep_objects",
    "Keep objects with tag",
    -1,
    functions
};

PyMODINIT_FUNC PyInit_keep_objects() {
    return PyModule_Create(&module);
}
