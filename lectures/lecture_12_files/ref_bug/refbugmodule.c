#include <Python.h>

static PyObject *ref_bug(PyObject* self, PyObject *args) {
    PyObject* list;

    if (!PyArg_ParseTuple(args, "O", &list))
        return NULL;

    PyObject *item = PyList_GetItem(list, 0);

    PyList_SetItem(list, 1, PyLong_FromLong(0L));
    PyObject_Print(item, stdout, 0);

    Py_RETURN_NONE;
}

static PyObject *ref_without_bug(PyObject* self, PyObject *args) {
    PyObject* list;

    if (!PyArg_ParseTuple(args, "O", &list))
        return NULL;

    PyObject *item = PyList_GetItem(list, 0);
    Py_INCREF(item);

    PyList_SetItem(list, 1, PyLong_FromLong(0L));
    PyObject_Print(item, stdout, 0);

    Py_DECREF(item);

    Py_RETURN_NONE;
}

static PyMethodDef BugMethods[] = {
    {"ref_bug",  ref_bug, METH_VARARGS, NULL},
    {"ref_without_bug",  ref_without_bug, METH_VARARGS, NULL},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef bugmodule = {
    PyModuleDef_HEAD_INIT,
    "ref_bug",
    NULL,
    -1,
    BugMethods
};

PyMODINIT_FUNC PyInit_ref_bug(void) {
    return PyModule_Create(&bugmodule);
}
