from setuptools import setup, Extension


setup(
    name='string_peek',
    version='1.0',
    description='Python Package with string_peek Extension',
    ext_modules=[
        Extension(
            'string_peek',
            sources=['stringpeekmodule.cpp'],
            py_limited_api=True)
    ],
)
