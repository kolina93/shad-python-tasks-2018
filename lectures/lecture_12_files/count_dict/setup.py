from distutils.core import setup, Extension

module1 = Extension('count_dict', sources = ['countdict.cpp'], extra_compile_args=['-Wno-write-strings'])

setup (name = 'PackageName',
       version = '1.0',
       description = 'This is a demo package',
       ext_modules = [module1])
