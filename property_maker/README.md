### Условие
Напишите метакласс `PropertyMaker`, который бы находил среди атрибутов класса методы, которые имеют вид
`get_<field>, set_<field>` и создавал бы `property` с именем `<field>` с `get_<field>` и `set_<field>` в качестве
getter-а и setter-а соответственно.
Метакласс не должен требовать, чтобы были определены и setter и getter (достаточно чего-то одного).

### Пример
```python
In [1]: from property_maker import PropertyMaker

In [2]: class C(metaclass=PropertyMaker):
   ...:     def get_x(self):
   ...:         print("get x")
   ...:         return self._x
   ...:     def set_x(self, value):
   ...:         print("set x")
   ...:         self._x = value
   ...:

In [3]: c = C()

In [4]: c.x = 1
set x

In [5]: print(c.x)
get x
1

In [6]:
```

### Пример запуска тестов
Запускаются тесты из файла ```test_public.py```.
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/property_maker$ pytest
```
