import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from test_utils import check_output


def test_example1():
    check_output(
        './reverse_index.py',

        # stdin
        '7\n'
        ' It tells the day of the month, and doesn t tell what o clock it is! \n'
        ' It IS a long tail, certainly,  said Alice, looking down with wonder at the Mouse s tail   but why do you call it sad  \n'
        'While she was trying to fix on one, the cook took the cauldron of soup off the fire, and at once set to work throwing everything within her reach at the Duchess and the baby  the fire irons came first  then followed a shower of saucepans, plates, and dishes.\n'
        'Alice folded her hands, and began:        You are old, Father William,  the young man said,      And your hair has become very white     And yet you incessantly stand on your head       Do you think, at your age, it is right  \n'
        ' It s all about as curious as it can be,  said the Gryphon.\n'
        'He looked anxiously over his shoulder as he spoke, and then raised himself upon tiptoe, put his mouth close to her ear, and whispered  She s under sentence of execution. \n'
        ' Come, let s try the first figure! \n'
        '13\n'
        'has old you and\n'
        'saucepans plates at fire within everything cauldron\n'
        'be\n'
        's let\n'
        'of upon anxiously whispered himself\n'
        'yet hair hair age age right your right\n'
        'o tells\n'
        'as\n'
        'he dishes at\n'
        'the the tells is are be william\n'
        's said gryphon it as it it\n'
        'fire the everything shower soup set\n'
        'a\n',

        # stdout
        '4\n'
        '3\n'
        '5\n'
        '7\n'
        '6\n'
        '4\n'
        '1\n'
        '5 6\n'
        '-1\n'
        '-1\n'
        '5\n'
        '3\n'
        '2 3\n'
    )
