## Обратный индекс

### Условие
Есть набор предложений, которые представляют из себя комбинацию слов, состоящих только из латинских букв, знаков препинания и пробельных символов. Нужно научится отвечать на запросы вида: в каких предложениях встречаются все заданные слова с точностью до регистра букв.

### Формат ввода
В первой строке число `n` - количество предложений (`1<=n<=1000`). Далее следуют `n` строк предложений. На следующей строке записано число `m` (`1<=m<=1000`) - количество запросов. Далее идут `m` строк запросов: каждый запрос это слова, состоящие только из латинских букв, разделенные через пробел. Количество слов в каждом запросе не превосходит `10`.

### Формат вывода
Нужно вывести `m` строк: на строке `i` нужно вывести через пробел индексы тех предложений (считая от `1`), в которых присутствуют все заданные слова с точностью до регистра букв. Если таких предложений нет, то нужно вывести `-1`.

### Пример
```bash
kolina93@kolina93-Latitude-E7440:~/shad/test-tasks/reverse_index$ cat > input.txt
7
 It tells the day of the month, and doesn t tell what o clock it is! 
 It IS a long tail, certainly,  said Alice, looking down with wonder at the Mouse s tail   but why do you call it sad  
While she was trying to fix on one, the cook took the cauldron of soup off the fire, and at once set to work throwing everything within her reach at the Duchess and the baby  the fire irons came first  then followed a shower of saucepans, plates, and dishes.
Alice folded her hands, and began:        You are old, Father William,  the young man said,      And your hair has become very white     And yet you incessantly stand on your head       Do you think, at your age, it is right  
 It s all about as curious as it can be,  said the Gryphon.
He looked anxiously over his shoulder as he spoke, and then raised himself upon tiptoe, put his mouth close to her ear, and whispered  She s under sentence of execution. 
 Come, let s try the first figure! 
13
has old you and
saucepans plates at fire within everything cauldron
be
s let
of upon anxiously whispered himself
yet hair hair age age right your right
o tells
as
he dishes at
the the tells is are be william
s said gryphon it as it it
fire the everything shower soup set
a
kolina93@kolina93-Latitude-E7440:~/shad/test-tasks/reverse_index$ ~/shad/tasks/venv/bin/python reverse_index.py < input.txt 
4
3
5
7
6
4
1
5 6
-1
-1
5
3
2 3
kolina93@kolina93-Latitude-E7440:~/shad/test-tasks/reverse_index$
```
