## Снова палиндромы

### Условие
Дана строка. Нужно определить минимальное количество символов, которое нужно добавить к строке слева или справа, чтобы она стала палиндромом.

### Формат ввода
Строка длиной не больше `1000` символов.

### Формат вывода
Вывести одно число: минимальное количество символов, которое нужно добавить к строке слева или справа, чтобы она стала палиндромом.

### Пример
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/palindromes$ cat input.txt 
AwI
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/palindromes$ python palindromes.py < input.txt 
2
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/palindromes$ 
```
