from .merge_sort import merge_sort


def test_simple_1():
    assert list(merge_sort([8, 6, 7, 9])) == [[6, 8, 7, 9], [6, 7, 8, 9]]


def test_simple_2():
    assert list(merge_sort([2, 1])) == [[1, 2]]


def test_simple_3():
    assert list(merge_sort([2, 1, 5, 8, 11, 12, 0, 1])) == [
        [1, 2, 5, 8, 11, 12, 0, 1], [1, 2, 5, 8, 0, 1, 11, 12], [0, 1, 1, 2, 5, 8, 11, 12]
    ]
