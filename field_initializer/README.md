### Условие
Иногда бывает утомительно писать код при инициализации инстанса класса.

```python
def __init__(self, value=value):
    self.value = value
```

Если полей много - это унылый процесс. Нужно написать метакласс `FieldInitializer`, который бы избавлял от этой рутины.
Метакласс должен брать все keyword аргументы инициализатора и создавать в инстансе класса атрибуты с соответствующими
именами и значениями.

Более формально, то если вы передаете в аргумент конструктора класса именованные аргументы, которых нет в его сигнатуре,
то нужно сначала создать объект класса, передав в его конструктор все аргументы, соответствующие сигнатуре `__init__`
(как позиционные, так и именованные), и после этого проставить те атрибуты, соотвествующие всем переданным пользователем
именованным аргументам, которые еще не установлены у объекта.

### Пример
```python
In [1]: from field_initializer import FieldInitializer

In [2]: class A(metaclass=FieldInitializer):
   ...:     pass
   ...:
   ...:

In [3]: a = A(foo=42)

In [4]: a.foo
Out[4]: 42

In [5]: class B(metaclass=FieldInitializer):
   ...:     def __init__(self, a, b, c, d=1):
   ...:         self.a = 1
   ...:         self.b = b
   ...:         self.d = d + 1
   ...:

In [6]: b = B(1, 2, 3, foo=4, d=5)

In [7]: b.a
Out[7]: 1

In [8]: b.b
Out[8]: 2

In [9]: b.foo
Out[9]: 4

In [10]: try:
    ...:     _ = b.c
    ...: except AttributeError:
    ...:     print("raise AttributeError as expected.")
    ...:
raise AttributeError as expected.

In [11]:
```

### Пример запуска тестов
Запускаются тесты из файла ```test_public.py```.
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/field_initializer$ pytest
```
