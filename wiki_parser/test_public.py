import json

import pytest

from .wiki_parser import WikipediaParser

TEST_CASES = [
    ('0.html', '0.a'),
    ('1.html', '1.a'),
]


@pytest.mark.parametrize('input_html,expected_output', TEST_CASES)
def test_wiki_parser(input_html, expected_output):
    with open(input_html, encoding='utf-8') as input_:
        html = input_.read()

    with open(expected_output, encoding='utf-8') as input_:
        expected = json.load(input_)

    parser = WikipediaParser()
    assert parser.parse(html) == expected
