import pytest

from .type_check import annotated_func, TypedList, TypedCallable, TypedAnnotatedCallable, type_check


# noinspection PyTypeChecker
def test_type_check():
    @annotated_func([int, float, list])
    def hello_func(x, y, z):
        print(f"Hello! Arguments are {x}, {y}, {z}")

    assert hello_func.__types__ == [int, float, list]

    assert isinstance([1, 2, 3], TypedList(int))
    assert not isinstance([1, 2, 3, 'a', None], TypedList(int))

    assert isinstance(print, TypedCallable())
    assert not isinstance(1, TypedCallable())

    assert isinstance(hello_func, TypedAnnotatedCallable([int, float, list]))
    assert not isinstance(hello_func, TypedAnnotatedCallable([int, float, str]))

    @annotated_func([int, float, TypedList(str)])
    def hello_func_2(x, y, z):
        print(f"Hello! Arguments are {x}, {y}, {z}")

    assert not isinstance(hello_func_2, TypedAnnotatedCallable([int, float, list]))
    assert isinstance(hello_func_2, TypedAnnotatedCallable([int, float, TypedList(str)]))

    @type_check
    @annotated_func([int, float, TypedList(str)])
    def hello_func_3(x, y, z):
        print(f"Hello! Arguments are {x}, {y}, {z}")

    with pytest.raises(TypeError):
        hello_func_3(1, 1.5, ['a', 'b', 1])
