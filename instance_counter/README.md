## Instance counter

### Условие
Реализуйте метакласс `InstanceSemaphore` такой, который следил бы за количеством инстансов классов, использующих его.
Количество инстансов указывается в свойстве класса `__max_instance_count__`. Обратите внимание, что число объектов
каждого класса регулируется отдельно.

### Пример
```python
In [1]: from semaphore import InstanceSemaphore

In [2]: class A(metaclass=InstanceSemaphore):
   ...:     __max_instance_count__ = 1
   ...:

In [3]: a = A()

In [4]: a
Out[4]: <__main__.A at 0x7f2ac6309fd0>

In [5]: try:
   ...:     A()
   ...: except TypeError:
   ...:     print("Raise TypeError as expected")
   ...:
Raise TypeError as expected

In [6]:
```

### Пример запуска тестов
Запускаются тесты из файла ```test_public.py```.
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/instance_counter$ pytest
```
