import heapq
import sys

from collections import deque
from typing import List, Tuple, Union


def graph_to_edges(n: int, graph: List[Tuple]) -> List[List[Union[int, Tuple[int, int]]]]:
    edges = [[] for _ in range(n)]

    for item in graph:
        if len(item) == 2:
            u, v = item
            u -= 1
            v -= 1
            edges[u].append(v)
            edges[v].append(u)
        else:
            u, v, c = item
            u -= 1
            v -= 1
            edges[u].append((v, c))
            edges[v].append((u, c))

    return edges


def bfs(n: int, graph: List[Tuple[int, int]], start_vertex: int) -> List[int]:
    result = [-1] * n
    edges = graph_to_edges(n, graph)

    vertexes_to_visit = deque()
    start_vertex -= 1
    vertexes_to_visit.append(start_vertex)
    result[start_vertex] = 0

    while vertexes_to_visit:
        current_vertex = vertexes_to_visit.popleft()

        for next_vertex in edges[current_vertex]:
            if result[next_vertex] != -1:
                continue
            vertexes_to_visit.append(next_vertex)
            result[next_vertex] = result[current_vertex] + 1

    return result


def dfs(n: int, graph: List[Tuple[int, int]], start_vertex: int) -> List[int]:
    sys.setrecursionlimit(100500)

    result = [-1] * n
    edges = graph_to_edges(n, graph)

    start_vertex -= 1

    order = []
    result[start_vertex] = 0

    def dfs_inner(vertex):
        order.append(vertex)

        for next_vertex in edges[vertex]:
            if result[next_vertex] == -1:
                result[next_vertex] = len(order)
                dfs_inner(next_vertex)

    dfs_inner(start_vertex)

    return result


def dijkstra(n: int, graph: List[Tuple[int, int, int]], start_vertex: int) -> List[int]:
    result = [-1] * n
    edges = graph_to_edges(n, graph)

    start_vertex -= 1
    distances = [(0, start_vertex)]
    values = [-1] * n
    values[start_vertex] = 0

    while distances:
        value, vertex = heapq.heappop(distances)

        if result[vertex] != -1:
            continue
        result[vertex] = value

        for next_vertex, cost in edges[vertex]:
            if values[next_vertex] == -1:
                values[next_vertex] = value + cost
                heapq.heappush(distances, (value + cost, next_vertex))
            elif values[next_vertex] > value + cost:
                values[next_vertex] = value + cost
                heapq.heappush(distances, (value + cost, next_vertex))

    return result
