# Инструкция по настройке окружения и сдаче заданий

## Настройка окружения

 1. Вам надо зарегистрироваться на [gitlab.com](https://gitlab.com/).
 
 2. Нужно зайти на [страницу](https://2018.shad-python-minsk.org/login), заполнить имя и фамилию и нажать на "Login to gitlab".
<div>
<img src="/.img/login.png"  alt="login" width="500">
</div>

 3. Вас перебросит на страницу, где нужно подтвердить доступ на чтение публичной информации из вашего аккаунта на гитлаб: логин, почта, имя и фамилия.
 Нужно нажать "Authorize".
 <div>
<img src="/.img/authorization.png"  alt="login" width="500">
</div>

 4. Надо зайти в свой репозиторий на https://gitlab.org и добавить публичный ssh-ключ в настройках профиля.
 Для того, чтобы сгенерировать ключ, используйте `ssh-keygen`

 5. Склонируйте и настройте репозиторий
 ```bash
 $ git clone https://gitlab.com/kolina93/shad-python-tasks-2018 $USERNAME
 $ cd $USERNAME
 $ git config user.name $USERNAME
 $ git config user.email $EMAIL
 $ git remote add student git@gitlab.com:shad-python-minsk-2018/$USERNAME.git
 ```

 6. Установите питоновские пакеты

 ```bash
 $ python --version
 Python 3.6.3
 $ pytest --version
 This is pytest version 3.2.3, ...
 $ pycodestyle --version
 2.4.0
 ```
 
 **Если не хотите ходить по граблям, проверьте, что версии у вас совпадают**

 Как установить:
 ```
 $ pip3 install --upgrade pycodestyle==2.4.0 pytest==3.2.3
 ```

## Сдача заданий

Для получения новых заданий надо делать `git pull`. Для локального тестирования кода вам понадобится библиотека `pytest`. Ее можно поставить через `pip`


Код относящийся к отдельной задаче находится в отдельной директории
(`hello_world` и т.д.). Там же находится условие задачи
(`hello_world/README.md`).

```bash
# Переходим в задачу
$ cd hello_world

# Пишем код в файле hello_world.py, реализовывая заданный интерфейс

# Запускаем юниттесты
hello_world$ pytest .

# Проверяем код-стайл
hello_world$ pycodestyle .

# Отправляем задачу в систему
hello_world$ python3 ../submit.py

# У скрипта есть флаг -v - если что-то не работает, попробуйте починить сами:w
```
Скрипт создаст новый коммит и запушит его в репозиторий. Вы
сможете наблюдать за результатами тестирования на странице `CI/CD -> Pipelines` в своём репозитории.

Там можно увидеть статусы посылок и результаты тестирования.
