import os

import telegram

from flask import Flask, request

app = Flask(__name__)

bot = telegram.Bot(token=os.environ['TOKEN'])
bot.setWebhook(f'https://{os.environ["URL"]}/echo')


@app.route('/echo', methods=['POST'])
def echo_telegram_handler():
    if request.method == 'POST':
        update = telegram.Update.de_json(request.get_json(force=True), bot)
        chat_id = update.message.chat.id
        bot.sendMessage(chat_id=chat_id, text=update.message.text)

    return 'ok'
